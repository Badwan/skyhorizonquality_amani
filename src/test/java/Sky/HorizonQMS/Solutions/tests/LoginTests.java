package Sky.HorizonQMS.Solutions.tests;
import Sky.HorizonQMS.Solutions.initialization.DBBase;
import Sky.HorizonQMS.Solutions.initialization.TestsBase;
import Sky.HorizonQMS.Solutions.pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class LoginTests extends TestsBase {

    private String userName, Password;
    LoginPage loginPage;

    @Test(description = "Test invalid user name and onvalid password")
    public void TestInvalidUserNadeanInvalidPassword()  {
        userName =  "Test1"; Password = "Test1";
        loginPage = new LoginPage(driver);
        loginPage.loginCases(userName, Password);
        String retunException = loginPage.getInvalidExceptionAlert().getText();
        Assert.assertEquals(retunException, "Invalid user name or password");
       }

    @Test(description = "Test valid user Name and valid password")
    public void TestValidUserNameValidPassword() throws FileNotFoundException, SQLException, ClassNotFoundException {
        loginPage = new LoginPage(driver);
        userName = "sky";
        Password= "s0ftware";
        loginPage.loginCases(userName, Password);
        WebElement navbar =driver.findElement(By.xpath("//a[@class='navbar-brand']//img"));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        boolean isNavbardiaplyed = navbar.isDisplayed();
        Assert.assertTrue(isNavbardiaplyed);

        String UserKey= DBBase.data("Select UserKey from users where UserID = 1").getString(1);
        Assert.assertNotEquals(UserKey,null );
    }

}
