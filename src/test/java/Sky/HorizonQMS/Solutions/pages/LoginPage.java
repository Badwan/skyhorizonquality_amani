package Sky.HorizonQMS.Solutions.pages;
import Sky.HorizonQMS.Solutions.initialization.TestsBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends TestsBase {
    @FindBy (id = "UserNameTextInput") WebElement userName;
    @FindBy (id = "PasswordInput") WebElement Password;
    @FindBy (xpath = "//input[@class='btn btn-primary btn-lg']") WebElement loginButton;
    @FindBy (id = "AlertLabel") WebElement invalidExceptionAlert;

    public LoginPage (WebDriver driver) {
      PageFactory.initElements(driver,this);
   }

    public WebElement getUserName() {
        return userName;
    }
    public WebElement getPassword() {
        return Password;
    }
    public WebElement getLoginButton() {
        return loginButton;
    }
    public WebElement getInvalidExceptionAlert() {
        return invalidExceptionAlert;
    }

    public void loginCases(String username, String password){
      getUserName().clear();
      getPassword().clear();
      getUserName().sendKeys(username);
      getPassword().sendKeys(password);
      getLoginButton().click();
}
}