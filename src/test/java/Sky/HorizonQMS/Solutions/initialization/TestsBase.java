package Sky.HorizonQMS.Solutions.initialization;
import utilities.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;

public class TestsBase {
    public static WebDriver driver;
    @BeforeTest
    public void beforetest () throws FileNotFoundException {
        String url = PropertiesReader.ReadData("url");
        String driverlocation = PropertiesReader.ReadData("DriverLocation");
        System.setProperty("webdriver.chrome.driver",driverlocation);
        driver = new ChromeDriver();
        driver.navigate().to(url);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @AfterTest
    public void afterTest()
    {
        driver.quit();
    }
}