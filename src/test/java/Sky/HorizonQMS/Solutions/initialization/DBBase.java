package Sky.HorizonQMS.Solutions.initialization;
import utilities.PropertiesReader;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBBase  {

    public static ResultSet data(String sqlquery) throws ClassNotFoundException, SQLException, FileNotFoundException {
        String DBServer = PropertiesReader.ReadData("DBServer");
        String DBUserName = PropertiesReader.ReadData("DBUserName");
        String DBPassword = PropertiesReader.ReadData("DBPassword");
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        // Creating a variable for the connection called "con"
        Connection con = DriverManager.getConnection(DBServer, DBUserName, DBPassword);
        // our query below
        PreparedStatement statement = con.prepareStatement(sqlquery);
        // creating variable to execute query
        ResultSet result = statement.executeQuery();
        result.next();
        return result;
    }
}


